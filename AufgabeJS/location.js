var pos;
var BHT = {lat: 52.5447318, lon: 13.3547022};

var image = 'https://img.icons8.com/nolan/64/000000/map-pin.png'

/*Aus www.w3schools.com/html/html5_geolocation.asp */
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

/*Aus https://www.w3schools.com/html/html5_geolocation.asp*/
function showPosition(position) {
    //save position to the variable pos   
   pos ={lat: position.coords.latitude, lng: position.coords.longitude};

   document.getElementById('demo').innerHTML =
   "Latitude: " + position.coords.latitude +
   "<br>Longitude: " + position.coords.longitude +
    
   //display the distance calculation
   "<br>Distance to the Mensa: "+getDistance(position.coords.latitude,position.coords.longitude,BHT.lat, BHT.lon) +" km " +
   "<br>Accuracy of the positon: "+position.coords.accuracy+"m";
    
    //show the map with current position    
   initMap();
}

/*https://developers.google.com/maps/documentation/javascript/adding-a-google-map */
// Initialize and add the map
function initMap() {
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: pos});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: pos, map: map, icon: image});
    marker.addListener('click', toggleBounce);
}


/* https://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates */
function getDistance(lat2,lon2,lat1,lon1) {
   var R = 6371; // Radius
   var dLat = deg2rad(lat2-lat1);  // deg2rad
   var dLon = deg2rad(lon2-lon1);
   var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
   var d = R * c; // Abstand in km
   return d.toFixed(3);
}

function deg2rad(deg) {
   return deg * (Math.PI/180)
}

